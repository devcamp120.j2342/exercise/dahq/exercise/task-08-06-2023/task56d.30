package com.devcamp.artistalbumapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.model.Artist;

import com.devcamp.artistalbumapi.service.ArtistService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ArtistController {
    @Autowired
    private ArtistService artistService;

    @GetMapping("/artists")
    public ArrayList<Artist> artistsList() {
        ArrayList<Artist> artists = artistService.allArtists();
        return artists;
    }

    @GetMapping("/artist-info")
    public Artist findArtist(@RequestParam(required = true, name = "id") int id) {
        Artist artist = new Artist();
        artist = artistService.findArtistById(id);
        return artist;
    }

    @GetMapping("/artists/{index}")
    public Artist artist(@PathVariable int index) {
        Artist artist = artistService.findArtistByIndex(index);
        return artist;
    }

}
