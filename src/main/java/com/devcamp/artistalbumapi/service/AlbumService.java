package com.devcamp.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.model.Album;

@Service
public class AlbumService {
    Album albumA1 = new Album(1, "A1");
    Album albumA2 = new Album(2, "A2");
    Album albumA3 = new Album(3, "A3");
    Album albumB1 = new Album(4, "B1");
    Album albumB2 = new Album(5, "B2");
    Album albumB3 = new Album(6, "B3");
    Album albumC1 = new Album(7, "C1");
    Album albumC2 = new Album(8, "C2");
    Album albumC3 = new Album(9, "C3");

    public ArrayList<Album> albumsA() {
        ArrayList<String> songs = new ArrayList<>();
        songs.add("Muon mang la tu luc");
        songs.add("Making My way");
        songs.add("on the time");
        albumA1.setSongs(songs);
        albumA2.setSongs(songs);
        albumA3.setSongs(songs);

        ArrayList<Album> albums = new ArrayList<>();
        albums.add(albumA1);
        albums.add(albumA2);
        albums.add(albumA3);
        return albums;

    }

    public ArrayList<Album> albumsB() {
        ArrayList<String> songs = new ArrayList<>();
        songs.add("Muon mang la tu luc");
        songs.add("Making My way");
        songs.add("Meeting");
        albumB1.setSongs(songs);
        albumB2.setSongs(songs);
        albumB3.setSongs(songs);
        ArrayList<Album> albums = new ArrayList<>();
        albums.add(albumB1);
        albums.add(albumB2);
        albums.add(albumB3);
        return albums;

    }

    public ArrayList<Album> albumsC() {
        ArrayList<String> songs = new ArrayList<>();
        songs.add("Muon mang la tu luc");
        songs.add("Making My way");
        songs.add("Meeting");
        albumC1.setSongs(songs);
        albumC2.setSongs(songs);
        albumC3.setSongs(songs);
        ArrayList<Album> albums = new ArrayList<>();
        albums.add(albumC1);
        albums.add(albumC2);
        albums.add(albumC3);
        return albums;

    }

    public Album finAlbumById(int paramId) {
        ArrayList<Album> albums = new ArrayList<>();
        albums.addAll(albumsA());
        albums.addAll(albumsB());
        albums.addAll(albumsC());

        Album albumResult = new Album();

        for (Album album : albums) {
            if (album.getId() == paramId) {
                albumResult = album;
            }

        }
        return albumResult;
    }

}
