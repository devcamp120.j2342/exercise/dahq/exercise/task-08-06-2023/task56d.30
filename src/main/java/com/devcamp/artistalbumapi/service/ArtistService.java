package com.devcamp.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.model.Artist;

@Service
public class ArtistService {
    Artist artistA = new Artist(1, "Artist A");
    Artist artistB = new Artist(2, "Artist B");
    Artist artistC = new Artist(3, "Artist C");
    Artist artistD = new Artist(4, "Artist D");
    Artist artistE = new Artist(5, "Artist E");
    Artist artistF = new Artist(6, "Artist F");
    @Autowired
    AlbumService albumService;

    public ArrayList<Artist> allArtists() {

        artistA.setAlbums(albumService.albumsA());
        artistB.setAlbums(albumService.albumsB());
        artistC.setAlbums(albumService.albumsC());
        artistD.setAlbums(albumService.albumsA());
        artistE.setAlbums(albumService.albumsB());
        artistF.setAlbums(albumService.albumsC());

        ArrayList<Artist> artists = new ArrayList<>();
        artists.add(artistA);
        artists.add(artistB);
        artists.add(artistC);
        artists.add(artistD);
        artists.add(artistE);
        artists.add(artistF);

        return artists;

    }

    public Artist findArtistById(int paramId) {
        ArrayList<Artist> artists = allArtists();
        Artist artistReult = new Artist();
        for (Artist artist : artists) {
            if (artist.getId() == paramId) {
                artistReult = artist;
            }

        }
        return artistReult;
    }

    public Artist findArtistByIndex(int paramIndex) {
        ArrayList<Artist> artists = allArtists();
        Artist artist = new Artist();
        if (paramIndex < 0 || paramIndex > 5) {
            return artist;
        } else {
            artist = artists.get(paramIndex);
        }
        return artist;
    }

}
